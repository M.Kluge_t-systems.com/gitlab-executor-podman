#!/bin/bash
#
#    gitlab-executor-podman
#    Copyright (C) 2021 John Goetz <theodore.goetz@gmail.com>
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <https://www.gnu.org/licenses/>.

set -eo pipefail
trap "exit $SYSTEM_FAILURE_EXIT_CODE" ERR

if [[ "${LOAD_BASHRC}" != "false" && -f ~/.bashrc ]]; then
    . ~/.bashrc
fi

podman rm --force --ignore "$CI_CONTAINER_NAME"
