#!/bin/bash
#
#    gitlab-executor-podman
#    Copyright (C) 2021 John Goetz <theodore.goetz@gmail.com>
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <https://www.gnu.org/licenses/>.

SCRIPT="$1"
STEP="$2"

set -eo pipefail
trap "exit $SYSTEM_FAILURE_EXIT_CODE" ERR
if [[ "${LOAD_BASHRC}" != "false" && -f ~/.bashrc ]]; then
    . ~/.bashrc
fi

#
# Order of steps intiated by the GitLab-CI system
#
# CI_HELPER_IMAGE (gitlab/gitlab-runner)
#    prepare_script
#    get_sources
#    restore_cache
#    download_artifacts
# CI_JOB_IMAGE (alpine)
#    step_*
#    build_script
#    step_*
#    after_script
# CI_HELPER_IMAGE (gitlab/gitlab-runner)
#    archive_cache OR archive_cache_on_failure
#    upload_artifacts_on_success OR upload_artifacts_on_failure
#    cleanup_file_variables
#

if [[
    "$STEP" == "prepare_script" || "$STEP" == "get_sources" ||
    "$STEP" == "restore_cache" || "$STEP" == "download_artifacts" ||
    "$STEP" == "archive_cache" || "$STEP" == "archive_cache_on_failure" ||
    "$STEP" == "upload_artifacts_on_success" ||
    "$STEP" == "upload_artifacts_on_failure" ||
    "$STEP" == "cleanup_file_variables"
]]; then
    USER_ID=$(podman exec "$CI_CONTAINER_NAME" id -u || echo 0)
    GROUP_ID=$(podman exec "$CI_CONTAINER_NAME" id -g || echo 0)

    if [[ ! -z $CI_HELPER_ENTRYPOINT ]]; then
        ENTRYPOINT=(--entrypoint $CI_HELPER_ENTRYPOINT)
    fi

    echo "Running $STEP in runner helper"
    if [[ $DEBUG -gt 0 ]]; then
        echo "podman run --rm --interactive --pull $CI_HELPER_IMAGE_PULL_POLICY \\"
        echo "    --volumes-from \"$CI_CONTAINER_NAME\" \\"
        echo "    --user ${USER_ID}:${GROUP_ID} ${ENTRYPOINT[@]} \\"
        echo "    $CI_HELPER_IMAGE ${CI_HELPER_CMD[@]} < \"$SCRIPT\""
    fi
    podman run --rm --interactive --pull $CI_HELPER_IMAGE_PULL_POLICY \
        --volumes-from "$CI_CONTAINER_NAME" \
        --user ${USER_ID}:${GROUP_ID} ${ENTRYPOINT[@]} \
        $CI_HELPER_IMAGE ${CI_HELPER_CMD[@]} < "$SCRIPT"
else
    echo "Running $STEP in $CI_CONTAINER_NAME"
    if [[ $DEBUG -gt 0 ]]; then
        echo "podman exec --interactive \"$CI_CONTAINER_NAME\" ${CI_JOB_CMD[@]} < \"$SCRIPT\""
    fi
    podman exec --interactive "$CI_CONTAINER_NAME" ${CI_JOB_CMD[@]} < "$SCRIPT"
fi

if [ $? -ne 0 ]; then
    exit $BUILD_FAILURE_EXIT_CODE
fi
