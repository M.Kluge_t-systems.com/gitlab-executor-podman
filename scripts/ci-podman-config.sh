#!/bin/bash
#
#    gitlab-executor-podman
#    Copyright (C) 2021 John Goetz <theodore.goetz@gmail.com>
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <https://www.gnu.org/licenses/>.

set -eo pipefail
trap "exit $SYSTEM_FAILURE_EXIT_CODE" ERR

ARGS=$(python -c $'
import argparse
import pathlib

home = pathlib.Path("~").expanduser()

parser = argparse.ArgumentParser()
parser.add_argument("--runner-builds-dir", default=str(home/"builds"))
parser.add_argument("--runner-cache-dir", default=str(home/"cache"))
parser.add_argument("--helper-image", default="gitlab/gitlab-runner:alpine")
parser.add_argument("--helper-image-pull-policy", default="missing")
parser.add_argument("--helper-entrypoint", default="/usr/bin/dumb-init")
parser.add_argument("--helper-cmd", default="/bin/bash")
parser.add_argument("--load-bashrc", default="false")

args = parser.parse_args()
print(args.runner_builds_dir, args.runner_cache_dir, args.helper_image,
      args.helper_image_pull_policy, args.helper_entrypoint, args.helper_cmd,
      args.load_bashrc,
      sep="\\n")
' "${@}")
readarray -t ARGS <<< "$ARGS"
LOAD_BASHRC="${ARGS[6]}"

if [[ "${LOAD_BASHRC}" != "false" && -f ~/.bashrc ]]; then
    . ~/.bashrc
fi

DIR="${ARGS[0]}"
DIR="$DIR/$CUSTOM_ENV_CI_RUNNER_SHORT_TOKEN"
DIR="$DIR/$CUSTOM_ENV_CI_CONCURRENT_ID"
CI_RUNNER_BUILDS_DIR="$DIR"

CI_RUNNER_CACHE_DIR="${ARGS[1]}"
CI_HELPER_IMAGE="${ARGS[2]}"
CI_HELPER_IMAGE_PULL_POLICY="${ARGS[3]}"
CI_HELPER_ENTRYPOINT="${ARGS[4]}"
CI_HELPER_CMD="${ARGS[5]}"

# container name
NAME="runner-$CUSTOM_ENV_CI_RUNNER_SHORT_TOKEN"
NAME="$NAME-$CUSTOM_ENV_CI_CONCURRENT_ID"
NAME="$NAME-project-$CUSTOM_ENV_CI_PROJECT_ID"
NAME="$NAME-$CUSTOM_ENV_CI_CONCURRENT_PROJECT_ID"
CI_CONTAINER_NAME="$NAME"

CI_BUILDS_DIR=${CUSTOM_ENV_CI_BUILDS_DIR:-/ci/builds}
CI_CACHE_DIR=${CUSTOM_ENV_CI_CACHE_DIR:-/ci/cache}

cat << EOS
{
  "driver": {
    "name": "podman",
    "version": "v1.1.0"
  },

  "builds_dir": "${CI_BUILDS_DIR}",
  "cache_dir": "${CI_CACHE_DIR}",
  "builds_dir_is_shared": false,

  "job_env": {
    "CI_RUNNER_BUILDS_DIR": "${CI_RUNNER_BUILDS_DIR}",
    "CI_RUNNER_CACHE_DIR": "${CI_RUNNER_CACHE_DIR}",

    "CI_HELPER_IMAGE": "${CI_HELPER_IMAGE}",
    "CI_HELPER_IMAGE_PULL_POLICY": "${CI_HELPER_IMAGE_PULL_POLICY}",
    "CI_HELPER_ENTRYPOINT": "${CI_HELPER_ENTRYPOINT}",
    "CI_HELPER_CMD": "${CI_HELPER_CMD}",

    "CI_REGISTRY_USER": "$CUSTOM_ENV_CI_REGISTRY_USER",
    "CI_REGISTRY_PASSWORD": "$CUSTOM_ENV_CI_REGISTRY_PASSWORD",
    "CI_REGISTRY": "$CUSTOM_ENV_CI_REGISTRY",

    "CI_CONTAINER_NAME": "${CI_CONTAINER_NAME}",

    "CI_JOB_IMAGE": "${CUSTOM_ENV_CI_JOB_IMAGE:-alpine}",
    "CI_JOB_IMAGE_PULL_POLICY": "${CUSTOM_ENV_CI_JOB_IMAGE_PULL_POLICY:-missing}",
    "CI_JOB_ENTRYPOINT": "${CUSTOM_ENV_CI_JOB_ENTRYPOINT}",
    "CI_JOB_CMD": "${CUSTOM_ENV_CI_JOB_CMD:-bash -l}",

    "CI_BUILDS_DIR": "${CI_BUILDS_DIR}",
    "CI_CACHE_DIR": "${CI_CACHE_DIR}",

    "CI_TIMEOUT": "$((2*24*60*60))",
    "DEBUG": "0"
  }
}
EOS
