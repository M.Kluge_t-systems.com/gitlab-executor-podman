#!/bin/bash
#
#    gitlab-executor-podman
#    Copyright (C) 2021 John Goetz <theodore.goetz@gmail.com>
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <https://www.gnu.org/licenses/>.

set -eo pipefail
trap "exit $SYSTEM_FAILURE_EXIT_CODE" ERR

if [[ "${LOAD_BASHRC}" != "false" && -f ~/.bashrc ]]; then
    . ~/.bashrc
fi

CURRENT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"
source ${CURRENT_DIR}/ci-podman-lib.sh

if [[ $DEBUG -gt 0 ]]; then
    echo "CI_RUNNER_BUILDS_DIR: $CI_RUNNER_BUILDS_DIR"
    echo "CI_RUNNER_CACHE_DIR: $CI_RUNNER_CACHE_DIR"
    echo "CI_BUILDS_DIR: $CI_BUILDS_DIR"
    echo "CI_CACHE_DIR: $CI_CACHE_DIR"

    echo "CI_HELPER_IMAGE: $CI_HELPER_IMAGE"
    echo "CI_HELPER_IMAGE_PULL_POLICY: $CI_HELPER_IMAGE_PULL_POLICY"
    echo "CI_HELPER_ENTRYPOINT:"
    printf "  %s\n" "${CI_HELPER_ENTRYPOINT[@]}"
    echo "CI_HELPER_CMD:"
    printf "  %s\n" "${CI_HELPER_CMD[@]}"

    echo "CI_JOB_IMAGE: $CI_JOB_IMAGE"
    echo "CI_JOB_IMAGE_PULL_POLICY: $CI_JOB_IMAGE_PULL_POLICY"
    echo "CI_JOB_ENTRYPOINT:"
    printf "  %s\n" "${CI_JOB_ENTRYPOINT[@]}"
    echo "CI_JOB_CMD:"
    printf "  %s\n" "${CI_JOB_CMD[@]}"
    echo "LOAD_BASHRC: ${LOAD_BASHRC}"
fi

vcheck 2.0 podman || exit $SYSTEM_FAILURE_EXIT_CODE

umask 000
mkdir -p "${CI_RUNNER_BUILDS_DIR}"
mkdir -p "${CI_RUNNER_CACHE_DIR}"
umask 002

ID=$(podman ps --quiet --no-trunc --filter "name=^$CI_CONTAINER_NAME\$" 2>/dev/null)
if [ "$ID" != "" ]; then
    echo "removing old container $CI_CONTAINER_NAME"
    podman rm --force --ignore $ID &>/dev/null
fi

echo "logging into container registry $CI_REGISTRY"
podman login -u "$CI_REGISTRY_USER" -p "$CI_REGISTRY_PASSWORD" "$CI_REGISTRY"

if [[ ! -z $CI_JOB_ENTRYPOINT ]]; then
    ENTRYPOINT=(--entrypoint $CI_JOB_ENTRYPOINT)
fi

echo "running container $CI_CONTAINER_NAME"
if [[ $DEBUG -gt 0 ]]; then
    echo "podman run --rm --detach --pull \"$CI_JOB_IMAGE_PULL_POLICY\" \\"
    echo "  --name \"$CI_CONTAINER_NAME\" \\"
    echo "  --volume ${CI_RUNNER_BUILDS_DIR}:${CI_BUILDS_DIR}:rw,z \\"
    echo "  --volume ${CI_RUNNER_CACHE_DIR}:${CI_CACHE_DIR}:rw,z \\"
    echo "  ${ENTRYPOINT[@]} $CI_JOB_IMAGE sleep $CI_TIMEOUT"
fi
ID=$(podman run --rm --detach --pull "$CI_JOB_IMAGE_PULL_POLICY" \
     --name "$CI_CONTAINER_NAME" \
     --volume ${CI_RUNNER_BUILDS_DIR}:${CI_BUILDS_DIR}:rw,z \
     --volume ${CI_RUNNER_CACHE_DIR}:${CI_CACHE_DIR}:rw,z \
     ${ENTRYPOINT[@]} $CI_JOB_IMAGE sleep $CI_TIMEOUT)
echo "container ID: $ID"

for i in $(seq 10); do
    if podman exec $ID echo hello &>/dev/null; then
        break
    fi
    if [ "$i" -eq 10 ]; then
        echo 'Waited for 10 seconds to start container, exiting..'
        podman rm --force --ignore $ID
        exit $SYSTEM_FAILURE_EXIT_CODE
    fi
    sleep 1s
done

install-command "$CI_CONTAINER_NAME" ${CI_JOB_CMD[@]}
