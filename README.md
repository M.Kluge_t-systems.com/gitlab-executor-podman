# gitlab-runner-podman

A GitLab custom runner using Podman containers.

* Honors `image` keyword for jobs in `.gitlab-ci.yml` configuraiton
* Persistent host-bound builds directories
* Host-local cache
* Configurable image pull policy

## Installation and Requirements

The individual scripts, found under the `scripts` directory, may be copied into
a location such as `/usr/local/bin` and pointed to during registration of the
runner with the `gitlab-runner register` command. The associated software
requirements are:

    * Bash
    * Python 3.5 or later
    * Podman 2.0 or later

## Usage

There are several parameters that are controlled by environment variables in
the executor. They deal mostly with handling of the cache and artifacts, but
also include the default image used for the job.

```shell
BINDIR=/usr/local/bin
RUNNER_TOPDIR=$HOME

gitlab-runner register                                      \
    --config ./gitlab-runner-config.toml                    \
    --tag-list `hostname -s`,podman,container,linux         \
    --non-interactive                                       \
    --name `hostname -s`-podman                             \
    --limit 4                                               \
    --url $CI_SERVER_URL                                    \
    --registration-token $REGISTRATION_TOKEN                \
    --executor custom                                       \
    --builds-dir /ci/builds                                 \
    --cache-dir /ci/cache                                   \
    --custom-config-exec $BINDIR/ci-podman-config.sh        \
    --custom-config-exec-timeout 3                          \
    --custom-prepare-exec $BINDIR/ci-podman-prepare.sh      \
    --custom-prepare-exec-timeout $(( 30 * 60 ))            \
    --custom-run-exec $BINDIR/ci-podman-run.sh              \
    --custom-cleanup-exec $BINDIR/ci-podman-cleanup.sh      \
    --custom-cleanup-exec-timeout $(( 5 * 60 ))             \
    --custom-graceful-kill-timeout $(( 1 * 60 ))            \
    --custom-force-kill-timeout $(( 3 * 60 ))
```

After adding options to set the location of the build and cache directories on
the runner host, the `toml` configuration file may look something like this:

```toml
[[runners]]
  name = "<hostname>-podman"
  limit = 4
  url = "<gitlab-server-url>"
  token = "<runner-token>"
  executor = "custom"
  builds_dir = "/ci/builds"
  cache_dir = "/ci/cache"
  [runners.custom]
    config_exec = "/usr/local/bin/ci-podman-config.sh"
    config_args = [
      "--runner-builds-dir", "<topdir>/builds",
      "--runner-cache-dir", "<topdir>/cache"
    ]
    config_exec_timeout = 3
    prepare_exec = "/usr/local/bin/ci-podman-prepare.sh"
    prepare_exec_timeout = 1800
    run_exec = "/usr/local/bin/ci-podman-run.sh"
    cleanup_exec = "/usr/local/bin/ci-podman-cleanup.sh"
    cleanup_exec_timeout = 300
    graceful_kill_timeout = 60
    force_kill_timeout = 180
```

A persistent `builds` directory is used that is unique to each concurrently
running job within the runner. The GitLab CI system runs the job from a
subdirectory that is unique to the project and branch. That is, the directory
under which the job operates is:
```
/path/to/builds/<runner-token>/<concurrent-id>/<namespace>/<project>/<branch>
```

The pull policy for the job image defaults to `missing` and gets passed to the
`--pull` argument of the `podman run` command. If pulling from a private
registry, you may consider setting this to `always` with the
`CI_JOB_IMAGE_PULL_POLICY` variable in the pipeline configuration to avoid the
need to manually update the images on each runner host.

The timeouts set are some initially reasonable values but may be completely
omitted if desired.

GitLab will run several steps before, during and after the actual job's script.
The preparation scripts are run using the official gitlab-runner container
(called the "helper" in this context but any container that has the
`gitlab-runner` executable in the `PATH` will do. The stages run and the image
used are summarized here, in order:

* gitlab/gitlab-runner
    * prepare_script
    * get_sources
    * restore_cache
    * download_artifacts
* CI_JOB_IMAGE
    * step_*
    * build_script
    * step_*
    * after_script
* gitlab/gitlab-runner
    * archive_cache OR archive_cache_on_failure
    * upload_artifacts_on_success OR upload_artifacts_on_failure
    * cleanup_file_variables

This runner honors the `image` keyword in the `.gitlab-ci.yaml` file. The image
is pulled if neccessary and started using the `sleep` command which must be
available in the image. If the command, which defaults to `bash`, is not found
in the job image, an attempt to install it will be performed as the `root` user
using several different package managers (see the function `install-command` in
`scripts/ci-podman-lib.sh` for details).

Each step of the run stage is executed in a login shell using the command:
```
    podman exec <container-name> bash -l < script
```

Finally, the container is removed forcefully during the cleanup stage.

## Features

All artifacts and cache facilities are supported in addition to a persistent
builds directory similar to the standard docker executor.

These are the environment variables that can be set in the CI pipeline
configuration that control how the job is executed:

* `CI_JOB_IMAGE`: The container image. (default: `alpine`)
* `CI_JOB_IMAGE_PULL_POLICY`: Passed to `podman run`'s `--pull` option.
  (default: `missing`)
* `CI_JOB_ENTRYPOINT`: The job's entrypoint. When unset, the image's entrypoint
  is honored.
* `CI_JOB_CMD`: The command which reads the contents of the job's scripts via
  stdin. (default: `bash -l`)
* `CI_BUILDS_DIR`: The top-level build directory within the container, which
  will contain the clone of the repository. (default: `/ci/builds`)
* `CI_CACHE_DIR`: The location of the cache directory within the container.
  (default: `/ci/cache`)

The following options are available for the `ci-podman-config.sh` script and
can be set using the `config_args` in the `[runners.custom]` section of the
runner configuration toml file:

* `--runner-builds-dir`: The host's top-level builds directory.
  (default: `~/builds`)
* `--runner-cache-dir`: The host's top-level directory. (default: `~/cache`)
* `--helper-image`: The image used to process artifacts, cache, git clone and
  cleanup. (default: `gitlab/gitlab-runner:alpine`)
* `--helper-image-pull-policy`: The policy passed to `podman run`'s `--pull`
  option when running the helper container. (default: `missing`)
* `--helper-entrypoint`: The container entrypoint for the helper image.
  (default: `/usr/bin/dumb-init`)
* `--helper-cmd`: The command which executed the various stages' scripts from
  stdin. (default: `/bin/bash`)

## Limitations and Implementation Notes

### Build Directory Relocation Causes CAfile Error

The builds directory within the container which corresponds to the top-level
directory where the repository is cloned and this may be specified using the
`CI_BUILDS_DIR` variable in the CI pipeline configuration. However, if the
repository was previously cloned with a different builds directory location,
the `git fetch` will fail because it can't find the "CAfile". As a result, care
should be taken when changing the value of `CI_BUILDS_DIR`. To ensure the
fetching of the repository is successful, you can set `GIT_STRATEGY`  to
`clone` in the CI configuration.

### Restration with `--custom-config-args` Does Not Split String Into List

The `--custom-config-args` command line option for `gitlab-runner register`
does not seem to allow an array input. The runner configuration toml file
should be edited by hand after registration to ensure proper configuration.

### Scripts Source `~/.bashrc` Directly

Each script will source the `~/.bashrc` file if it exists to ensure that the
environment podman is run from is consistent with the behavior when logging in
interactively.
